package com.tyler.flickrapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;

import java.util.Vector;

public class MyAdapter extends BaseAdapter {

    Vector<String> listUrl = new Vector<>();
    private Context context_;
    private LayoutInflater inflater;

    public MyAdapter(Context context) {
        context_ = context;
        this.inflater = LayoutInflater.from(context);
        Log.i("mYADAPTER","CREATION ");

    }

    //fonction qui ajoute chaque url des images dans une liste
    public void dd(String url){
        listUrl.add(url);
        Log.i("TYLER","Adding to adapter url : "+url);
    }

    @Override
    public int getCount() {
        return listUrl.size();
    }

    @Override
    public Object getItem(int position) {
        return listUrl.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.image_layout, null);

        /* TextView tv = convertView.findViewById(R.id.textViewList); //textview qui affiche les URL
        tv.setText(listUrl.get(position));*/

        ImageView iv = convertView.findViewById(R.id.bitmapDisplay);

        // Get a RequestQueue
        RequestQueue queue = MySingleton.getInstance(parent.getContext()).getRequestQueue();

        Response.Listener<Bitmap> rep_listener = response -> {
            iv.setImageBitmap(response);
        };

        Response.ErrorListener errorListener = response -> {
            Log.i("ERROR", "This image can't be download : "+ listUrl.get(position));
        };

        ImageRequest ir = new ImageRequest(listUrl.get(position), rep_listener, 2000,
                2000, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, errorListener);
        queue.add(ir);

        return convertView;
    }

}
