package com.tyler.flickrapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class ListActivity extends AppCompatActivity {

    private MyAdapter adapter;
    private String loc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ListView listview = (ListView)findViewById(R.id.list_main);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if(b!=null)
        {
            loc =(String) b.get("key_localisation");
        }

        adapter = new MyAdapter(this);
        AsyncFlickrJSONDataForList taskList = new AsyncFlickrJSONDataForList(ListActivity.this, adapter);
        Thread thread = new Thread() {
            public void run() {

                //String url = new String("https://www.flickr.com/services/feeds/photos_public.gne?tags=" + loadData() + "&format=json");
                String url= "https://api.flickr.com/services/rest/?method=flickr.photos.search&";
                url = url+"api_key=5c02cf619ada86a19f8340ea60a30e2e"+loc+"&text="+loadData()+"&format=json&nojsoncallback=1&per_page=15";

                taskList.onPostExecute(taskList.doInBackground(url));


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listview.setAdapter(adapter);
                    }
                });

            }
        };
        thread.start();
    }

    public String loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(Preferences.SHAREDPREFS, MODE_PRIVATE);
        String text = sharedPreferences.getString(Preferences.TEXT, "cats");
        return text;
    }
}


