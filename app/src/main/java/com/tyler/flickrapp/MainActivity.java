package com.tyler.flickrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button getImage;
    private Button goList;
    private Button goSetting_button;
    private String loc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getImage = (Button) findViewById(R.id.getAnImage);
        this.goList = (Button) findViewById(R.id.goToList);
        this.goSetting_button = (Button) findViewById(R.id.setting_button);

        //recherche la localisation
        Location bestLocation = askPosition();

        AsyncFlickrJSONData task = new AsyncFlickrJSONData(MainActivity.this);

        getImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread() {
                    public void run() {
                        //String url = new String("https://www.flickr.com/services/feeds/photos_public.gne?tags=" + loadData() + "&format=json");

                        if(bestLocation!=null){
                            loc = "&has_geo=1&lat="+bestLocation.getLatitude()+"&lon="+bestLocation.getLongitude();
                        }

                        String url= "https://api.flickr.com/services/rest/?method=flickr.photos.search&";
                        url = url+"api_key=5c02cf619ada86a19f8340ea60a30e2e"+loc+"&text="+loadData()+"&format=json&nojsoncallback=1&per_page=15";

                        //lancement de la fonction doInbackground, qui retournera un objet json, qui sera directement
                        // envoyer dans la fonction OnPostexecute pour afficher image par image à à chaque click
                        task.onPostExecute(task.doInBackground(url));
                    }
                };
                thread.start();
            }
        });

        //Pour accéder à l'activité listActivity qui affichera les images d'un sujet flickr listé
        goList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ListActivity.class);
                intent.putExtra("key_localisation", loc);
                startActivity(intent);
            }
        });

        //Pour accéder à l'activité permettant de paramétrer notre recherche flickr
        goSetting_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Preferences.class);
                startActivity(intent);
            }
        });
    }

     private Location askPosition() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 48 );
        }
         LocationManager mLocationManager;
         mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
         List<String> providers = mLocationManager.getProviders(true);
         Location bestLocation = null;
         for (String provider : providers) {
             Location l = mLocationManager.getLastKnownLocation(provider);
             if (l == null) {
                 continue;
             }
             if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                 // Found best last known location: %s", l);
                 bestLocation = l;
             }
         }
         if(bestLocation==null){
             return bestLocation;
         }
         /*Log.i("tyler","Latitude" +
                 bestLocation.getLatitude());
         Log.i("tyler","Longitude" +
                 bestLocation.getLongitude());*/
         return bestLocation;
    }

    //fonction permettant de recharger l'état du dernier sujet flickr recherché
    public String loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(Preferences.SHAREDPREFS, MODE_PRIVATE);
        String text = sharedPreferences.getString(Preferences.TEXT, "cats");
        return text;
    }
}