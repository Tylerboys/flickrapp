package com.tyler.flickrapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {

    private Context context_;
    private MyAdapter adapter;
    private URL url;

    AsyncFlickrJSONDataForList(){
    }

    public AsyncFlickrJSONDataForList(Context context, MyAdapter adapter) {
        context_ = context;
        this.adapter = adapter;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {

        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream

            result = readStream(in); // Read stream
        }
        catch (MalformedURLException e) { e.printStackTrace(); }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json; // returns the result
    }

    protected void onPostExecute(JSONObject s) {
        try {
            for (int i = 0; i<s.getJSONObject("photos").getJSONArray("photo").length(); i++){
                String id = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("id");
                String server = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("server");
                String secret = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("secret");
                Log.i("TYLER", i+" URL media: https://live.staticflickr.com/" + server+"/"+id+"_"+secret+".jpg");
                String url = "https://live.staticflickr.com/" + server+"/"+id+"_"+secret+".jpg";
                adapter.dd(url);
                adapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        //String jsonextracted = sb.toString();
        //return jsonextracted.substring("jsonFlickrFeed(".length(), sb.length() - 1);
        return sb.toString();
    }
}

