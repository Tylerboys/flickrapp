package com.tyler.flickrapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Preferences extends AppCompatActivity {

    private EditText editText;
    private Button button;
    private TextView textView;

    private String text;

    public static final String SHAREDPREFS = "sharedPrefs";
    public static final String TEXT = "text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preference_layout);

        editText= (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        textView= (TextView) findViewById(R.id.textView);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                textView.setText(editText.getText().toString());
                saveData();
            }
        });
        loadData();
        upadateViews();
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHAREDPREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TEXT, textView.getText().toString());
        editor.apply();
    }

    //Fonction qui affichera sur la page de setting le topic choisi
    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHAREDPREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");
    }

    //Fonction qui affichera sur la page de setting le topic choisi
    public void upadateViews(){
        textView.setText(text);
    }
}
