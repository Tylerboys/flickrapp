package com.tyler.flickrapp;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

    private AppCompatActivity activity;
    private int i = 0;

    public AsyncFlickrJSONData(){
    }

    public AsyncFlickrJSONData(AppCompatActivity mainActivity) {
        activity = mainActivity;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {

        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream

            result = readStream(in); // Read stream
        }
        catch (MalformedURLException e) { e.printStackTrace(); }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json; // returns the result
    }

    protected void onPostExecute(JSONObject s) {
        try {
            String id = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("id");
            String server = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("server");
            String secret = ((JSONObject)s.getJSONObject("photos").getJSONArray("photo").get(i)).getString("secret");
            Log.i("TYLER", i+" URL media: https://live.staticflickr.com/" + server+"/"+id+"_"+secret+".jpg");
            String url = "https://live.staticflickr.com/" + server+"/"+id+"_"+secret+".jpg";
            // Downloading image
            AsyncBitmapDownloader abd = new AsyncBitmapDownloader();
            abd.execute(url);
            Bitmap bm = abd.doInBackground(url);
            changeImage(bm);

            i++;    //incrémentation de i jusqu'à ce que toutes les images soient affichés
            if(i==(s.getJSONObject("photos").getJSONArray("photo")).length()){
                i=0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Fonction qui va changer l'image apparaissant dans le layout et pour accéder aux modification on
    // utilise un runOnUiThread qui va faire éxécuter le code dans le thread principal
    public void changeImage(Bitmap bm) {
        final Bitmap bmFinal = bm;
        ImageView iv = (ImageView) activity.findViewById((R.id.imageView));
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iv.setImageBitmap(bmFinal);
            }
        });
    }

    //fonction readStream qui va renvoyer le contenu json de la page flickr, en retirant l'élément "jsonFlickrFeed(" du début et la parenthèse de fin
    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();
        //String jsonextracted = sb.toString();
        //return jsonextracted.substring("jsonFlickrApi(".length(), sb.length() - 1);
        return sb.toString();
    }

}
